<?php

namespace App\Service;

use App\Entity\ConnectionTable;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\Table;

class DatabaseDiffer
{
    private int $previewSize;

    public function __construct(int $previewSize = 100)
    {
        $this->previewSize = $previewSize;
    }

    public function buildConnection($url): Connection
    {
        return DriverManager::getConnection(['url' => $url]);
    }

    public function listTables(Connection $connection): array
    {
        $tables = array_map(
            fn (Table $table) => $table->getName(),
            $connection->createSchemaManager()->listTables(),
        );

        sort($tables);

        return $tables;
    }

    public function listPrimaryKeyColumns(ConnectionTable $ct): array
    {
        foreach ($ct->connection()->createSchemaManager()->listTableIndexes($ct->table()) as $index) {
            if (!$index->isPrimary()) { continue; }
            return $index->getColumns();
        }
        throw new \Exception('Cannot determine primary key');
    }

    public function listColumns(ConnectionTable $ct): array
    {
        return $ct->connection()->createSchemaManager()->listTableColumns($ct->table());
    }

    public function compare(ConnectionTable $before, ConnectionTable $after, string $where = ''): array
    {
        $rowsDiff = $this->compareRows($before, $after, $where);

        $columnsDiff = $this->compareColumns($before, $after);

        $cellsDiff = $this->compareCells($before, $after, $where, $rowsDiff['both']);

        $rowsDiff['commonCount'] = count($rowsDiff['both']);
        unset($rowsDiff['both']);

        $columnsDiff['commonCount'] = count($columnsDiff['both']);
        unset($columnsDiff['both']);

        $cellsDiff['commonCount'] = $rowsDiff['commonCount'] * $columnsDiff['commonCount'];

        return [
            'rows' => $rowsDiff,
            'columns' => $columnsDiff,
            'cells' => $cellsDiff,
        ];
    }

    public function compareRows(ConnectionTable $before, ConnectionTable $after, string $where): array
    {
        $rowsBefore = $this->query($before, $where, true);
        $rowsAfter = $this->query($after, $where, true);

        $removed = [];
        $added = [];
        $both = [];

        foreach ($rowsBefore as $keyBefore => $valueBefore) {
            if (!array_key_exists($keyBefore, $rowsAfter)) {
                $removed[] = $valueBefore;
            } else {
                $both[$keyBefore] = true;
            }
        }

        foreach ($rowsAfter as $keyAfter => $valueAfter) {
            if (!array_key_exists($keyAfter, $rowsBefore)) {
                $added[] = $valueAfter;
            }
        }

        return [
            'countBefore' => count($rowsBefore),
            'countAfter' => count($rowsAfter),
            'removed' => [
                'count' => count($removed),
                'preview' => array_slice($removed, 0, $this->previewSize),
            ],
            'added' => [
                'count' => count($added),
                'preview' => array_slice($added, 0, $this->previewSize),
            ],
            'both' => $both,
        ];
    }

    public function compareColumns(ConnectionTable $before, ConnectionTable $after): array
    {
        $beforeColumns = $this->listColumns($before);
        $afterColumns = $this->listColumns($after);

        $added = [];
        $removed = [];
        $both = [];

        foreach ($beforeColumns as $beforeColumn => $beforeColumnDefinition) {
            if (!array_key_exists($beforeColumn, $afterColumns)) {
                $removed[] = $beforeColumn;
            } else {
                $both[] = $beforeColumn;
            }
        }

        foreach ($afterColumns as $afterColumn => $afterColumnDefinition) {
            if (!array_key_exists($afterColumn, $beforeColumns)) {
                $added[] = $afterColumn;
            }
        }

        return [
            'added' => $added,
            'removed' => $removed,
            'both' => $both,
        ];
    }

    public function compareCells(ConnectionTable $before, ConnectionTable $after, string $where, array $both): array
    {
        $rowsBefore = $this->query($before, $where, false);
        $rowsAfter = $this->query($after, $where, false);

        $diffs = [];
        $cellDiffsCount = 0;
        $diffsColumns = [];

        $previews = [];
        $previewColumnCounts = [];

        foreach ($rowsBefore as $key => $valueBefore) {
            if (!array_key_exists($key, $both)) { continue; }
            $valueAfter = $rowsAfter[$key];
            $rowDiff = $this->compareRow($valueBefore, $valueAfter);
            if (count($rowDiff)) {
                $value = [
                    'before' => $valueBefore,
                    'after' => $valueAfter,
                    'changes' => $rowDiff,
                ];
                $diffs[$key] = $value;
                $cellDiffsCount += count($rowDiff);
                foreach ($rowDiff as $column) {
                    if (!array_key_exists($column, $diffsColumns)) { $diffsColumns[$column] = 0; }
                    $diffsColumns[$column]++;
                    if (!array_key_exists($column, $previewColumnCounts)) { $previewColumnCounts[$column] = 0; }
                    if ($previewColumnCounts[$column] < $this->previewSize) {
                        $previewColumnCounts[$column]++;
                        $previews[$key] = $value;
                    }
                }
            }
        }

        arsort($diffsColumns);

        return [
            'rowsCount' => count($diffs),
            'cellsCount' => $cellDiffsCount,
            'columnsDiffsCounts' => $diffsColumns,
            'preview' => $previews,
        ];
    }

    private function query(ConnectionTable $ct, string $where, bool $onlyPK): array
    {
        $pk = $this->listPrimaryKeyColumns($ct);
        $pkStr = join(', ', array_map(fn($id) => $ct->connection()->quoteIdentifier($id), $pk));

        $rows = $ct->connection()->prepare(sprintf(
            'SELECT %s FROM %s %s ORDER BY %s',
            $onlyPK ? $pkStr : '*',
            $ct->connection()->quoteIdentifier($ct->table()),
            $where ? 'WHERE ' . $where : '',
            $pkStr
        ))->executeQuery()->fetchAllAssociative();

        $hashmap = [];
        foreach ($rows as $row) {
            $key = array_filter(
                $row,
                fn ($key) => in_array($key, $pk),
                ARRAY_FILTER_USE_KEY
            );
            $hashmap[join('>|<', $key)] = $row;
        }

        return $hashmap;
    }

    private function compareRow(array $before, array $after): array
    {
        $changes = [];
        foreach ($before as $key => $valueBefore) {
            if (!array_key_exists($key, $after)) { continue; }
            $valueAfter = $after[$key];
            if ($valueBefore !== $valueAfter) {
                $changes[] = $key;
            }
        }

        return $changes;
    }
}
