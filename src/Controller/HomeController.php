<?php

namespace App\Controller;

use App\Entity\ConnectionTable;
use App\Service\DatabaseDiffer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig');
    }

    /**
     * @Route("/api/tables")
     */
    public function tables(Request $request, DatabaseDiffer $differ): Response
    {
        return $this->json($differ->listTables($differ->buildConnection($request->get('url'))));
    }

    /**
     * @Route("/api/pk/{url}/{table}")
     */
    public function primaryKey(DatabaseDiffer $differ, string $url, string $table): Response
    {
        return $this->json($differ->listPrimaryKeyColumns($differ->buildConnection($url), $table));
    }

    /**
     * @Route("/api/compare")
     */
    public function compare(Request $request, DatabaseDiffer $differ): Response
    {
        ini_set('max_execution_time', (int) $request->get('timeLimit', 0));
        ini_set('memory_limit', $request->get('memoryLimit', '1G'));

        return $this->json($differ->compare(
            new ConnectionTable(
                $differ->buildConnection($request->get('before')['url']),
                $request->get('before')['table'],
            ),
            new ConnectionTable(
                $differ->buildConnection($request->get('after')['url']),
                $request->get('after')['table'],
            ),
            $request->get('where'),
        ));
    }
}
