<?php

namespace App\Entity;

use Doctrine\DBAL\Connection;

class ConnectionTable
{
    private Connection $connection;
    private string $table;

    public function __construct(
        Connection $connection,
        string $table
    ) {
        $this->connection = $connection;
        $this->table = $table;
    }

    public function connection(): Connection
    {
        return $this->connection;
    }

    public function table(): string
    {
        return $this->table;
    }
}
