install:
	composer install
	yarn

start: stop
	symfony serve -d
	symfony run -d yarn dev-server

stop:
	symfony server:stop

deploy:
	composer install --no-dev --optimize-autoloader
	yarn
	yarn build
