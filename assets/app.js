import './styles/app.scss';

import 'bootstrap/js/src/dropdown';

import Vue from 'vue';
import App from './components/App.vue';

Vue.prototype.$t = (key, params = {}) => Translator.trans(key, params);

Vue.prototype.$post = async (url, data) => {
    const res = await fetch(url, {
        method: 'POST',
        headers: {'content-type': 'application/json'},
        body: JSON.stringify(data)
    });

    return await res.json();
}

Vue.prototype.$formatNumber = (number) => number.toLocaleString('en-US');

Vue.prototype.$eventHub = new Vue();

new Vue({
    el: '#app',
    components: { App },
    template: '<App/>',
});
