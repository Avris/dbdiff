class Session {
    get(key, fallback = null) {
        const value = localStorage.getItem(key);
        return value === null
            ? fallback
            : JSON.parse(value);
    }

    set(key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    }
}

export default new Session();
